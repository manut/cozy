Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cozy
Upstream-Contact: Julian Geywitz <cozy@geigi.de>
Source: https://github.com/geigi/cozy

Files: *
Copyright: 2017-2024 Julian Geywitz <cozy@geigi.de>
License: GPL-3+

Files: cozy/control/mpris.py
Copyright: 2023, Benedek Dévényi <rdbende@proton.me>
 2017, Julian Geywitz <cozy@geigi.de>
 2016, Gaurav Narula
 2016, Felipe Borges <felipeborges@gnome.org>
 2014-2017, Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 2013, Vadim Rutkovsky <vrutkovs@redhat.com>
 2013, Arnel A. Borja <kyoushuu@yahoo.com>
License: GPL-3+

Files: data/icons/*
Copyright: 2023 Benedek Dévényi <rdbende@proton.me>
 2017-2021 Julian Geywitz <cozy@geigi.de>
 2020, Jakub Steiner <jimmac@gmail.com>
 2019, Nararyans R.I. <fnri39@gmail.com>
License: CC0-1.0

Files: debian/*
Copyright: 2024, Manuel Traut <manut@mecka.net>
License: GPL-3+

License: CC0-1.0
 On Debian systems the full text of the CC0-1.0 license can be found in
 /usr/share/common-licenses/CC0-1.0

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
